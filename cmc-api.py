#

# Let's get this party started!
from wsgiref.simple_server import make_server

import os
import falcon
import json
import yaml
import peerplays
from peerplays.peerplays import PeerPlays
from peerplays.asset import Asset
from peerplays.amount import Amount

def sats_to_fixed(amount, P):
    V = int(amount) / 10**P
    return "{:.{prec}f}".format(V, prec=P)

def get_supplies(asset):
    P = asset["precision"]
    max_supply = sats_to_fixed(asset["options"]["max_supply"], P)
    ddo = asset.blockchain.rpc.get_objects([asset["dynamic_asset_data_id"]])[0]
    current_supply = sats_to_fixed(ddo["current_supply"], P)
    return {
        "id": asset["id"],
        "symbol": asset["symbol"],
        "maximum": max_supply,
        "total": max_supply,
        "circulating": current_supply
    }

class SupplyResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        args = dict(req.params)
        sym_or_id = args.get("asset","1.3.0")
        A = Asset(sym_or_id)
        supplies = get_supplies(A)
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
        resp.text = json.dumps(supplies)


class MaxSupplyResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        args = dict(req.params)
        sym_or_id = args.get("asset","1.3.0")
        A = Asset(sym_or_id)
        supply = get_supplies(A)["maximum"]
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_TEXT
        resp.text = supply


class TotalSupplyResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        args = dict(req.params)
        sym_or_id = args.get("asset","1.3.0")
        A = Asset(sym_or_id)
        supply = get_supplies(A)["total"]
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_TEXT
        resp.text = supply


class CirculatingSupplyResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        args = dict(req.params)
        sym_or_id = args.get("asset","1.3.0")
        A = Asset(sym_or_id)
        supply = get_supplies(A)["circulating"]
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_TEXT
        resp.text = supply


class RichListResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        args = dict(req.params)
        sym_or_id = args.get("asset","1.3.0")
        limit = int(args.get("num","20"))
        A = Asset(sym_or_id)
        P = A["precision"]
        richlist = A.blockchain.rpc.get_asset_holders(sym_or_id, 0, limit, api="asset")
        result = [
            {
                **{k:i[k]
                   for k in ["name","account_id"]},
                "balance": sats_to_fixed(i["amount"],P),
                "symbol": A["symbol"]
            }
            for i in  richlist
        ]
        #result = [[i["name"], i["account_id"], sats_to_fixed(i["amount"],P)] for i in richlist]
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
        resp.text = json.dumps(result)

# falcon.App instances are callable WSGI apps...
# in larger applications the app is created in a separate file
app = falcon.App()

# Resources are represented by long-lived class instances
supply = SupplyResource()
max_supply = MaxSupplyResource()
total_supply = TotalSupplyResource()
circulating_supply = CirculatingSupplyResource()
top_balances = RichListResource()

if __name__ == '__main__':
    working_dir = os.path.dirname(os.path.abspath(__file__))
    swagger_config = os.path.join(working_dir, 'conf/swagger.yaml')
    app_config = os.path.join(working_dir, 'conf/explorer-conf.yaml')
    config = yaml.safe_load(open(app_config, 'r'))

    def route_cat(route):
        # Add prefix to route if needed
        return '{}{}'.format(config["url-prefix"], route)

    app.add_route(route_cat('/supply'), supply)
    app.add_route(route_cat('/max_supply'), max_supply)
    app.add_route(route_cat('/total_supply'), total_supply)
    app.add_route(route_cat('/circulating_supply'), circulating_supply)
    app.add_route(route_cat('/top_balances'), top_balances)

    P = PeerPlays(node=config["peerplays-api-node"])
    peerplays.instance.set_shared_blockchain_instance(P)

    from swagger_ui import falcon_api_doc
    docs_url = route_cat('/docs')
    falcon_api_doc(app, config_path=swagger_config, url_prefix=docs_url, editor=True)

    with make_server(config["listen-host"], config["listen-port"], app) as httpd:
        print(
            'Serving on host %s port %d...' %
            (config["listen-host"],config["listen-port"])
        )
        print(
            'Swagger-UI at: http://%s:%d%s' %
            (config["listen-host"],config["listen-port"],docs_url)
        )
        # Serve until process is killed
        httpd.serve_forever()
