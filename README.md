# MarketCap API

Simple and concise REST API to retrieve supply data and token rich lists on Peerplays networks. Responses are structured to conform to requirements of major market cap aggregator sites.  This is not a comprehensive block explorer for general public use, but rather is for automated polling by aggregator sites.

## Setup:


```
cd conf
cp -i explorer-conf-sample.yaml explorer-conf.yaml
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

Edit `conf/explorer-conf.yaml` for your situation.  (In particular, this API needs the url of a Peerplays core node API with the "assets" API enabled.)

## Run:

```
source env/bin/activate
python3 ./cmc-api.py
```

## Usage:

The API is explained via a Swagger-UI interface.  After running the API, point browser at http://[host]:[port]/apidocs
